import { createRouter, createWebHistory } from 'vue-router';

const routes = [

  {
    path: "/products",
    name: "PageProductVue",
    component: () => import("@/views/pages/pageProducts.vue")
  },

  
  {
    path: "/detailsProduct/:id",
    name: "DetailsProduct",
    component: () => import("@/views/pages/detailsProduct.vue"),
    props: true, 
  },

    
  {
    path: "/cart",
    name: "CartPageVue",
    component: () => import("@/views/pages/cartPage.vue"),
    props: true, 
  },



];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
